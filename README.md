# How-To Use
use Build.ps1 like:

Build.ps1 -TargetFolder "C:\Project"  -ReleaseFolder "C:\Release" 

* where TargetFolder - your local folder, where repo were downloaded
** ReleaseFolder - Release Folder

# Exercise
Manual:
Create a Git repo locally or any remote one (github for example)
Add there 1 C++ and 2 C# projects where one of C# projects references another one

Automation:
1Get code from Git repo to a local folder
2 Modify project setting to enable debug symbols in release configuration of the projects
3 Build code in release configuration with any build engine/script (msbuild for example)
4 Calculate output files hashes for all binaries/assemblies and make hash/files manifest (xml or json for example)
5 Make a zip archive including all binaries/assemblies and generated manifest (use 7zip, Windows built in or any other)
6 Copy resulted zip to any release location
7 Copy resulted pdbs into a separate folder called Symbols in the same release location, saving original folder hierarchy

# Requirements
MSBuild.exe version is 16.2.37902.0 (MSBuild -version)
dotnet SDK must be install

For build use Build.ps1 Powershell sctipt 
Powershell  Version is 5.1.17763.592  (get-host)


    

