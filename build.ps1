param(
    [string]$TargetFolder,
    [string]$RepoUri = "https://gitlab.com/surb/exercise.git",
    [string]$ReleaseFolder
)

$TargetFolderProj = $TargetFolder + "\"	

function Get-SlnCodeFromRepo {
    if (-not (Test-Path $TargetFolder)) {
        Write-Host "Creating new Folder"$TargetFolder  -ForegroundColor Green
        New-Item -ItemType directory -Path  $TargetFolder
    }
    else {
        Write-Host "FilePath Set "$TargetFolder  -ForegroundColor Green
    }
    try {
        git clone $RepoUri $TargetFolder
    }
    catch { Write-Host $error[0] }
    write-host  "Repo  downloaded successfully" -ForegroundColor Green
    break
}

function BuildingCode {
    try {
        $proj = Get-ChildItem -Path $TargetFolderProj -Include   *.vcxproj, *.csproj -Recurse
        foreach ($rpojects in $proj) {	
            write-host "Building "$rpojects.name -ForegroundColor green
            dotnet msbuild /m:2 /t:restore,build,publish $rpojects /p:DebugSymbols=true /p:Configuration=Release /v:q /p:WarningLevel=0
            write-host $rpojects.name "Build Done"  -ForegroundColor Green
        }
    }
    catch { Write-Host $error[0] }
    break
}

function Make-PubFilesArch {
    $PublishFolder = $TargetFolderProj + "Builds"
    New-Item -ItemType directory -Path $PublishFolder
    $f = Get-ChildItem  -Path $TargetFolderProj -Directory -Recurse -Filter "publish" | Where-Object { $_.FullName -notmatch "obj" -And $_.FullName -match "release" }
    foreach ($folder in $f) {
        $pub = $folder.fullname.Replace($TargetFolderProj, "")
        $ItemPath = $PublishFolder + "\" + $pub.Split("\")[0]
        $ItemPath 
        New-Item -ItemType directory -Path $ItemPath
        Copy-Item -Path $folder.fullname -Destination  $ItemPath -Recurse
    }
    $DllFilepath = $TargetFolderProj + "\Dll1\Release\Dll1"
    Copy-Item -Path  $DllFilepath  -Destination  $PublishFolder"\dll" -Recurse
    $arch = $TargetFolderProj + "Artifacts.Zip"
    Compress-Archive -Path $PublishFolder -Update -DestinationPath  $arch
}
function  Get-PubFilesHash {
    $hashFileName = $PublishFolder + "HashResult.json"
    $hash = Get-ChildItem  -Path $PublishFolder  -Recurse | Get-FileHash
    $hash | ConvertTo-Json | Out-File $hashFileName
    write-host  "Hash result in " $hashFileName -ForegroundColor Green
}
function Copy-PubFilesArch {
    if (-not (Test-Path $ReleaseFolder)) {
        Write-Host "Creating new Folder"$ReleaseFolder  -ForegroundColor Green
        New-Item -ItemType directory -Path  $ReleaseFolder
    }
    else {
        Write-Host "FilePath Set "$ReleaseFolder  -ForegroundColor Green
    }
    Copy-Item -Path  $arch -Destination $ReleaseFolder -Recurse
}

function Copy-SlnSymbol {
    $SymbolsFolder = $ReleaseFolder + "\Symbols"
    New-Item -ItemType directory -Path $SymbolsFolder
    $PDBDllFilepath = $TargetFolderProj + "\Dll1\Release\*"
    Get-ChildItem -Path $PDBDllFilepath -Include *.pdb -Recurse | Copy-Item -Destination $SymbolsFolder 
}

write-host  "Get code from Git repo to a local folder " -ForegroundColor Green
write-host  "***************************************** " -ForegroundColor Green
Get-SlnCodeFromRepo

write-host  "* Build code " -ForegroundColor Green
write-host  "***************************************** " -ForegroundColor Green
BuildingCode

write-host  "* Make a zip archive" -ForegroundColor Green
write-host  "***************************************** " -ForegroundColor Green
Make-PubFilesArch

write-host  "Calculate output files hashes"-ForegroundColor Green
write-host  "***************************************** " -ForegroundColor Green
Get-PubFilesHash

write-host  "Copy resulted zip to any release location" -ForegroundColor Green
write-host  "***************************************** " -ForegroundColor Green
Copy-PubFilesArch

write-host  "Copy resulted pdbs" -ForegroundColor Green
write-host  "***************************************** " -ForegroundColor Green
Copy-SlnSymbol

